// Uses Node, AMD or browser globals to create a module.

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.returnExports = factory();
  }
}(this, function () {

    function Queue() {
        this.tail = [];
        this.head = [].slice.call(arguments);
        this.offset = 0;
    }

    Queue.prototype.shift = function () {
        if (this.offset === this.head.length) {
            var tmp = this.head;
            tmp.length = 0;
            this.head = this.tail;
            this.tail = tmp;
            this.offset = 0;
            if (this.head.length === 0) return;
        }
        return this.head[this.offset++];
    };

    Queue.prototype.push = function () {
        return [].push.apply(this.tail, arguments);
    };

    Object.defineProperty(Queue.prototype, "length", {
        get: function () {
            return this.head.length - this.offset + this.tail.length;
        },
        enumerable: false,
        configurable: true
    });    

    return Queue;
}));
